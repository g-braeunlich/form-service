# Quickstart

1. clone
2. `virtualenv .`
4. `. bin/activate`
5. `pip install aiohttp`
6. `python webservice.py`

## Testing

```bash
curl -X POST -H "Content-Type: application/json" --data '{"a": "b"}' localhost:8080
```
