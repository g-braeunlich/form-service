#!/usr/bin/env python3
"""html form webservice"""

import json
import os
from http import HTTPStatus
import urllib.parse
import logging

from aiohttp import web

SERVER_ADDRESS = (os.environ.get("HOST", ""), int(os.environ.get("PORT", 4785)))
STATIC_DIR = os.path.join(os.path.dirname(__file__), "static")


async def post_handler(request):
    """
    Coroutine given to the server, st. it knows what to do with an HTTP request.

    This one handles a POST, checks its content, and forwards it to the matrix room.
    """
    data = await request.read()
    content_type = request.content_type or guess_content_type(data)
    if content_type == "application/x-www-form-urlencoded":
        try:
            data = urllib.parse.parse_qs(data, strict_parsing=True, max_num_fields=1)
            data = {key: value[0] for key, value in data.items()}
        except ValueError as e:
            logging.error(f"Invalid urlencoded data: {e}\n" + data)
            return create_json_response(
                HTTPStatus.BAD_REQUEST, "Invalid urlencoded data"
            )
    else:
        try:
            data = json.loads(data)
        except json.decoder.JSONDecodeError as e:
            logging.error(f"Invalid json: {e}\n" + data)
            return create_json_response(HTTPStatus.BAD_REQUEST, "Invalid JSON")

    print(data)
    # do something with data here

    return create_json_response(HTTPStatus.OK, "OK")


def create_json_response(status, ret):
    """Create a JSON response."""
    response_data = {"status": status, "ret": ret}
    return web.json_response(response_data, status=status)


def guess_content_type(payload: str) -> str:
    """Based on the payload, guess the content type.

    Either application/json or application/x-www-form-urlencoded."""
    if payload.startswith("{"):
        return "application/json"
    return "application/x-www-form-urlencoded"


if __name__ == "__main__":
    app = web.Application()
    app.add_routes(
        [web.post("/", post_handler), web.static("/", path=STATIC_DIR, show_index=True)]
    )
    web.run_app(app)
